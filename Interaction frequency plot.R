#!/usr/bin/env Rscript

#This script plots the interaction frequency between bins before and and after
normalization
library(gplots)
setwd("~/Desktop/50kb_resolution_intrachromosomal")
pdf('RAWtableplot.pdf')
for(chr in paste0('chr', c(1:22,"X"))){
HMtable <- read.table(paste0('./', chr, "/MAPQGE30/", chr,"_50kb.RAWobserved")) #raw
contanct matrix
plot(HMtable[HMtable$V3>10000,1:2], main=paste0("RAW_", chr))
}
#
dev.off()
pdf('KRtableplot.pdf')
for(chr in paste0('chr', c(1:22,"X"))){
HMtable <- read.table(paste0('./', chr, "/MAPQGE30/KRscript_output.tsv"))
#KR_normalized contact matrix
plot(HMtable[HMtable$V3>10000,1:2], main=paste0("KR_", chr))
}
dev.off()

