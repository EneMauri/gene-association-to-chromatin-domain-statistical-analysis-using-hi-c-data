#!/usr/bin/env Rscript

#This script draws a heat map of the percentiles per chromosome across the genome
library(gplots)
setwd("~/Desktop/50kb_resolution_intrachromosomal")
pdf('RAWvsKRnormQuantiles_plot.pdf')#the output image
quantile_matrix<-NULL
for(chr in paste0('chr', c(1:22,"X"))){
HMtable=read.table(paste0('./', chr, "/MAPQGE30/", chr,"_50kb.RAWobserved")) #reads
raw contact matrix
quantile_matrix=cbind(quantile_matrix, quantile(HMtable$V3, seq(0, 1, 0.01)))
#concatenates the different chromosome percentiles
}
for(chr in paste0('chr', c(1:22,"X"))){
HMtable=read.table(paste0(paste0('./', chr, "/MAPQGE30/KRscript_output.tsv"))) #reads
normalized contact matrix
quantile_matrix=cbind(quantile_matrix, quantile(HMtable$V3, seq(0, 1, 0.01)))
}
heatmap(quantile_matrix[101:1,], NA, NA, scale='row') #it does a heat map of the
percentiles
dev.off()

