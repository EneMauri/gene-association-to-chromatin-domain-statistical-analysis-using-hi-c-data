#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <map>
#include <cmath>
#include <stdlib.h>
using namespace std;

int main (int argc, char** argv) {

  char *datafile, *normfile;
  string usage, line;
  float val, norma;
  int x, y, binSize, vecline, normBin1, normBin2;

  usage = 
    "USAGE: xyz2normalizeLieberman xyz_RawData.txt KRnorm.txt binSize\n"
    "       xyz_RawData.txt: Observed raw data in three columns format\n"
    "       KRnorm.txt: normalization vector\n"
    "       binSize: number of bp per bin\n";

  if (argc != 4) { cout << usage << endl; exit(1); }

  datafile = argv[1]; // Observed raw data
  ifstream ifs(datafile); if(!ifs) { cout << "Error opening " << datafile << endl; exit(1); }

  normfile = argv[2]; // KRnorm normalization vector
  ifstream ifs2(normfile); if(!ifs2) { cout << "Error opening " << normfile << endl; exit(1); }

  binSize = atoi(argv[3]);

  map<int, float> norm;
  map<int, int> isdata;

  vecline = 1;
  while (getline(ifs2, line)) {
    istringstream iss(line);
	iss >> val;
    if (!isnan(val)) {
      norm[vecline] = val;
      isdata[vecline] = 1;
    }
    vecline++;
  }
  
  while (getline(ifs, line)) {
	istringstream iss(line);
	iss >> x >> y >> val;
    normBin1 = ((x/binSize)+1);
    normBin2 = ((y/binSize)+1);
    if (isdata[normBin1] && isdata[normBin2]) {
      norma = norm[normBin1] * norm[normBin2];
      if (norma > 0) cout << fixed << x << "\t" << y << "\t" << val/norma << endl;
    }
  }

  return 0;
}
